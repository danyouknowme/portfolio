import { Route, Routes } from 'react-router-dom';
import Index from './pages/Index';
import Project from './pages/Project';

export default function App() {
  return (
    <div className='App'>
      <Routes>
        <Route path='/' element={<Index />}></Route>
        <Route path='/projects' element={<Project />}></Route>
        <Route path='*' element={<Index />}></Route>
      </Routes>
    </div>
  );
}
