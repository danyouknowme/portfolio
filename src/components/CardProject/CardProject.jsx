import './card-project.scss';
import { useState } from 'react';

export default function CardProject() {
  const [height, setHeight] = useState(0);

  window.onscroll = () => {
    setHeight(100 - window.pageYOffset / 10);
    console.log(height);
    return () => window.onscroll === null;
  };

  return (
    <div className='card'>
      <div className='hide' style={{ height: `${height}%` }}></div>
    </div>
  );
}
